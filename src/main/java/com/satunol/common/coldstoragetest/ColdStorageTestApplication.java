package com.satunol.common.coldstoragetest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@SpringBootApplication
@EntityScan(basePackages = {"com.satunol.common.security", "com.satunol.common.coldstoragetest"})
@ComponentScan(basePackages = {"com.satunol.common.security", "com.satunol.common.coldstoragetest"})
@EnableJpaRepositories(basePackages = {"com.satunol.common.security", "com.satunol.common.coldstoragetest"})
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ColdStorageTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ColdStorageTestApplication.class, args);
	}

}
