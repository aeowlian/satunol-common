package com.satunol.common.coldstoragetest.PerfMeasurement;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PerfMeasurementRepository extends JpaRepository<PerfMeasurement, Long> {
    List<PerfMeasurement> findByDetails_Id(Integer id);

	List<PerfMeasurement> findByDetails_TypeAndPerfMeasurementTimeBetween(String perfObjType, ZonedDateTime startDate, ZonedDateTime endDate);
}
