package com.satunol.common.coldstoragetest.PerfMeasurement;

import java.time.ZonedDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.satunol.common.coldstoragetest.ManagedObject.ManagedObject;
import com.satunol.common.coldstoragetest.PerfObject.PerfObject;

@Entity
public class PerfMeasurement {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String perfMeasurementValue;
    private ZonedDateTime perfMeasurementTime;

    @OneToOne
    private ManagedObject managedObject;

    @OneToOne
    private PerfObject details;

    public Long getId() {
        return id;
    }

    public PerfObject getDetails() {
        return details;
    }

    public void setDetails(PerfObject details) {
        this.details = details;
    }

    public ManagedObject getManagedObject() {
        return managedObject;
    }

    public void setManagedObject(ManagedObject managedObject) {
        this.managedObject = managedObject;
    }

    public ZonedDateTime getPerfMeasurementTime() {
        return perfMeasurementTime;
    }

    public void setPerfMeasurementTime(ZonedDateTime perfMeasurementTime) {
        this.perfMeasurementTime = perfMeasurementTime;
    }

    public String getPerfMeasurementValue() {
        return perfMeasurementValue;
    }

    public void setPerfMeasurementValue(String perfMeasurementValue) {
        this.perfMeasurementValue = perfMeasurementValue;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
 