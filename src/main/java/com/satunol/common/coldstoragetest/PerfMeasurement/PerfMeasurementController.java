package com.satunol.common.coldstoragetest.PerfMeasurement;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Repository
@CrossOrigin("*")
@RequestMapping("/perf-measurement")
public class PerfMeasurementController {

    @Autowired
    private PerfMeasurementRepository perfMeasurementRepository;
    
    @GetMapping("/perfObject/{perfObjId}")
    @PreAuthorize("@authorityEvaluator.hasPermission('read:performance:*')")
    @PostAuthorize("@perfMeasurementAuthorityEvaluator.hasPermission('read:performance:<location>', returnObject)")
    public ResponseEntity<?> getPerfMeasurementByDetails(@PathVariable int perfObjId) {
        List<PerfMeasurement> perfMeasurement = perfMeasurementRepository.findByDetails_Id(perfObjId);
        return ResponseEntity.ok(perfMeasurement);
    }

    @GetMapping("/perfObject-type")
    @PreAuthorize("@authorityEvaluator.hasPermission('read:performance:*')")
    @PostAuthorize("@perfMeasurementAuthorityEvaluator.hasPermission('read:performance:<location>', returnObject)")
    public ResponseEntity<?> getPerfMeasurementByDetailsType(
        @RequestParam String type,
        @RequestParam @DateTimeFormat(iso = ISO.DATE_TIME) ZonedDateTime startDate,
        @RequestParam @DateTimeFormat(iso = ISO.DATE_TIME) ZonedDateTime endDate
    ) {
        List<PerfMeasurement> perfMeasurement = 
                perfMeasurementRepository
                    .findByDetails_TypeAndPerfMeasurementTimeBetween(type, startDate, endDate);
        return ResponseEntity.ok(perfMeasurement);
    }
}
