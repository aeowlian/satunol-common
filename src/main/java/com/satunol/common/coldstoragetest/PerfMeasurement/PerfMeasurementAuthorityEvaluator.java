package com.satunol.common.coldstoragetest.PerfMeasurement;

import java.util.Iterator;
import java.util.List;

import com.satunol.common.security.configuration.AuthorityEvaluator;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class PerfMeasurementAuthorityEvaluator extends AuthorityEvaluator {
    
    public boolean hasPermission(String permission, ResponseEntity<List<PerfMeasurement>> response) {
        boolean isAllowed = false;
        System.out.println("==========");
        List<PerfMeasurement> measurements = (List<PerfMeasurement>)response.getBody();
        Iterator<PerfMeasurement> i = measurements.iterator();
        while (i.hasNext()) {
            PerfMeasurement measurement = i.next();
            System.out.println(permission);
            System.out.println(measurement.getManagedObject().getLocation().getName());
            boolean hasPermission = super.hasPermission(permission, "<location>", measurement.getManagedObject().getLocation().getName());
            if(hasPermission) {
                isAllowed = true;
            }else{
                measurements.remove(measurement);
            }
        }
        return isAllowed;
    }
}
