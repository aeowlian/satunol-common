package com.satunol.common.coldstoragetest.ManagedObject;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ManagedObjectRepository extends JpaRepository<ManagedObject, Integer> {
    
}
