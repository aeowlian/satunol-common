package com.satunol.common.coldstoragetest.PerfObject;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PerfObjectRepository extends JpaRepository<PerfObject, Integer> {
    
}
