package com.satunol.common.coldstoragetest.PerfObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/perf-object")
public class PerfObjectController {

    @Autowired
    private PerfObjectRepository perfObjectRepository;
    
    @GetMapping("/type/{perfId}")
    public ResponseEntity<?> getPerfTypeByPerfId(@PathVariable Integer perfId) {
        PerfObject object = perfObjectRepository.findById(perfId).get();
        return ResponseEntity.ok(object.getType());
    }
}
