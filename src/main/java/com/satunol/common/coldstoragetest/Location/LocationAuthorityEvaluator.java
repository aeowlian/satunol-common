package com.satunol.common.coldstoragetest.Location;

import com.satunol.common.security.configuration.AuthorityEvaluator;

import org.springframework.stereotype.Component;

@Component
public class LocationAuthorityEvaluator extends AuthorityEvaluator {
    
    public boolean hasPermission(String permission, String location) {
        return super.hasPermission(permission, "<location>", location);
    }
}
