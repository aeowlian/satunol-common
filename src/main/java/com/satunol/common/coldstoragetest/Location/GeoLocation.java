package com.satunol.common.coldstoragetest.Location;

import java.math.BigDecimal;

import javax.persistence.Embeddable;

@Embeddable
public class GeoLocation {
    private BigDecimal latitude;
    private BigDecimal longitude;

    public GeoLocation() {}

    public GeoLocation(BigDecimal latitude, BigDecimal longitude) {
        this.setLatitude(latitude);
        this.setLongitude(longitude);
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }
}
 