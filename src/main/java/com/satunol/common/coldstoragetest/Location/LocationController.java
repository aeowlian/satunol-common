package com.satunol.common.coldstoragetest.Location;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/location")
public class LocationController {

    @Autowired
    private LocationRepository locationRepository;
    
    @GetMapping("")
    @PreAuthorize("@authorityEvaluator.hasPermission('read|create|update|delete:location:*')")
    public ResponseEntity<?> getAll() {
        List<Location> locations = locationRepository.findAll();
        return ResponseEntity.ok(locations); 
    }

    @GetMapping("/{id}")
    @PreAuthorize("@authorityEvaluator.hasPermission('read|create|update|delete:location:*')")
    public ResponseEntity<?> getLocation(@PathVariable Integer id) {
        Location location = locationRepository.findById(id).get();
        return ResponseEntity.ok(location); 
    }

    @PostMapping("")
    @PreAuthorize("@authorityEvaluator.hasPermission('create:location:*')")
    public ResponseEntity<?> create(@RequestBody Location location) {
        locationRepository.saveAndFlush(location);
        return ResponseEntity.ok(location);
    }

    @PutMapping("")
    @PreAuthorize("@authorityEvaluator.hasPermission('update:location:*')")
    public ResponseEntity<?> update(@RequestBody Location location) {
        locationRepository.saveAndFlush(location);
        return ResponseEntity.ok(location);
    }

    @DeleteMapping("")
    @PreAuthorize("@authorityEvaluator.hasPermission('delete:location:*')")
    public ResponseEntity<?> delete(@RequestBody Location location) {
        locationRepository.delete(location);
        return ResponseEntity.ok(location);
    }
}
