package com.satunol.common.coldstoragetest.Inventory;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface InventoryRepository extends JpaRepository<Inventory, Integer> {
    
    Optional<Inventory> findById(int id);

}
