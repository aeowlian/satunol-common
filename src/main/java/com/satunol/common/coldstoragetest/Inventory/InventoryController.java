package com.satunol.common.coldstoragetest.Inventory;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/inventory")
public class InventoryController {

    @Autowired
    private InventoryRepository inventoryRepository;
    
    @GetMapping("/all")
    @PreAuthorize("@authorityEvaluator.hasPermission('read|create|delete:*:*')")
    public ResponseEntity<?> getAll(Authentication authentication) {
        List<Inventory> listOfInventory = inventoryRepository.findAll();
        return ResponseEntity.ok(listOfInventory);
    }

    @PostMapping("")
    public ResponseEntity<?> add(@RequestBody Inventory inventory) {
        return ResponseEntity.ok(inventoryRepository.save(inventory)); 
    }

    // @GetMapping("/site_work/{id}")
    // @PreAuthorize("@assignmentAuthorityEvaluator.hasPermissionWithSiteWorkRegion('*:*:<region>', #id, #listOfSubordinate)")
    // public List<Assignment> findBySiteWork(@PathVariable Integer id, @RequestParam(required = false, value = "", defaultValue = "") List<Party> listOfSubordinate) {
    //     SiteWork siteWork = new SiteWork();
    //     siteWork.setId(id);
    //     return assignmentRepo.findBySiteWork(siteWork);
    // }
}
