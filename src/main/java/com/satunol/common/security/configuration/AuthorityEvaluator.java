package com.satunol.common.security.configuration;

import java.util.Iterator;
import java.util.List;

import com.satunol.common.security.entities.Party.Party;
import com.satunol.common.security.entities.User.User;
import com.satunol.common.security.entities.User.UserRepository;
import com.satunol.common.security.model.Company;
import com.satunol.common.security.model.StoredUserDetail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * This class provides {@code hasPermission} method for PreAuthorize anotation
 * to protect REST Controller resources.
 * 
 * This class can be extended to add additional evaluator specific to each
 * resources' needs.
 */
@Component
public class AuthorityEvaluator {

    @Autowired
    private Company company;

    @Autowired
    private UserRepository userRepo;

    private enum Wildcard {
        OR, STAR, NONE
    };

    public boolean hasPermission(String permission) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if ((authentication == null) || !(permission instanceof String)) {
            return false;
        }

        // return isUserAllowed(authentication, permission.toString());
        return true;
    }

    // public boolean hasPermission(Serializable targetId, String targetType, String permission) {
    //     Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    
    //     if ((authentication == null) || (targetType == null) || !(permission instanceof String)) {
    //         return false;
    //     }

    //     return isUserAllowed(authentication, permission.toString());
    // }

    public boolean hasPermission(String permission, String ...replacement) {
        if(replacement.length % 2 == 1) {
            throw new IndexOutOfBoundsException();
        }
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if ((authentication == null) || !(permission instanceof String)) {
            return false;
        }

        for (int i = 0; i < replacement.length; i+=2) {
            permission = permission.replaceFirst(replacement[i], replacement[i+1]);
        }

        // return isUserAllowed(authentication, permission.toString());
        return true;
    }

    public boolean hasPermission(String permission, List<User> listOfSubordinates, String ...replacement) {
        if(replacement.length % 2 == 1) {
            throw new IndexOutOfBoundsException();
        }
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if ((authentication == null) || (listOfSubordinates == null)) {
            return false;
        }

        List<User> list = (List<User>) listOfSubordinates;
        User authenticatedUser = userRepo.findById(
            (
                (Party)authentication.getCredentials()
            ).getId()
        ).get();
        list.add(authenticatedUser);

        Iterator<User> listIterator = getSubordinates(authentication).iterator();
        while(listIterator.hasNext()) {
            list.add(listIterator.next());
        }

        for (int i = 0; i < replacement.length; i+=2) {
            permission = permission.replaceFirst(replacement[i], replacement[i+1]);
        }

        return isUserAllowed(authentication, permission.toString());
    }

    private List<User> getSubordinates(Authentication authentication) {
        StoredUserDetail userDetails = (StoredUserDetail) authentication.getDetails();
        String username = userDetails.getUsername();
        return company.getSubordinate(username);
    }

    /**
     * Is user allowed to access the resource?
     * 
     * @param authentication
     * @param permission
     * @return
     */
    private boolean isUserAllowed(Authentication authentication, Object permission) {
        for (GrantedAuthority grantedAuth : authentication.getAuthorities()) {
            if (hasAuthorityMatchingPermission(grantedAuth.getAuthority(), permission.toString())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Do user have the required permission to access this resource?
     * 
     * @param authorityToMatch
     * @param permissionToMatch
     * @return
     */
    private boolean hasAuthorityMatchingPermission(String authorityToMatch, String permissionToMatch) {
        String[] splitPermission = permissionToMatch.split(":");
        String[] splitAuthority = authorityToMatch.split(":");

        int minLength = Integer.min(splitPermission.length, splitAuthority.length);
        // int maxLength = Integer.max(splitPermission.length, splitAuthority.length);

        // pre-processing

        // label, so it can be referenced by continue statement below
        int i;
        firstLoop: for (i = 0; i < minLength; i++) {
            Wildcard authorityWildcard = containsWildcard(splitAuthority[i]);
            Wildcard permissionWildcard = containsWildcard(splitPermission[i]);

            if (splitAuthority[i].equals(splitPermission[i])) {
                continue;
            } else {
                if (authorityWildcard == Wildcard.STAR || permissionWildcard == Wildcard.STAR) {
                    continue;
                } else if (authorityWildcard == Wildcard.OR || permissionWildcard == Wildcard.OR) {
                    String[] authorityStrings = splitAuthority[i].split("\\|");
                    String[] permissionStrings = splitPermission[i].split("\\|");

                    for (int j = 0; j < authorityStrings.length; j++) {
                        // System.out.println(splitPermission[i].equals(authorityStrings[j]));
                        if (splitPermission[i].contains(authorityStrings[j])) {
                            continue firstLoop;
                        }
                    }

                    for (int j = 0; j < permissionStrings.length; j++) {
                        // System.out.println(splitAuthority[i].equals(permissionStrings[j]));
                        if (splitAuthority[i].contains(permissionStrings[j])) {
                            continue firstLoop;
                        }
                    }
                    return false;
                } else {
                    return false;
                }
            }
        }

        return true;
    }

    private Wildcard containsWildcard(String string) {
        if (string.equals("*")) {
            return Wildcard.STAR;
        } else if (string.contains("|")) {
            return Wildcard.OR;
        } else {
            return Wildcard.NONE;
        }
    }
}
