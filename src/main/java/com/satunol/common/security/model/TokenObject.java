package com.satunol.common.security.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "token")
public class TokenObject {
    
    @Id
    private String nonce;
    private String jwtString;

    public String getNonce() {
        return nonce;
    }

    public String getJwtString() {
        return jwtString;
    }

    public void setJwtString(String jwtString) {
        this.jwtString = jwtString;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }
}
