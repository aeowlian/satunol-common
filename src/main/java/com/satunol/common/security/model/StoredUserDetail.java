package com.satunol.common.security.model;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import com.satunol.common.security.entities.Authorities.Authorities;
import com.satunol.common.security.entities.Permission.Permission;
import com.satunol.common.security.entities.User.User;
import com.satunol.common.security.entities.User.UserRepository;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityCoreVersion;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class StoredUserDetail implements UserDetails {

    private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;
    private String email;
    private User user;

    public StoredUserDetail(String email, UserRepository userRepository) {
        this.email = email;
        Optional<User> userById = userRepository.findByUsername(email);
        if(userById.isPresent()) {
            user = userById.get();
        }else{
            user = userRepository.findByEmail(this.email);
        }
    }

    public User getUser() {
        return user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new LinkedList<GrantedAuthority>();

        // Break down roles in database to build granted authority
        // TODO change back to this lines of code when the user model is fixed
        // String[] roles = user.getRoles().split(",");
        // for (int i = 0; i < roles.length; i++) {
        //     GrantedAuthority authority = new SimpleGrantedAuthority(roles[i]);
            // authorities.add(authority);
        // }

        // Temporary solution for current user model
        Authorities userAuthorities = user.getAuthorities();
        for (Permission authority : userAuthorities.getPermissions()) {
            GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(authority.getPermission());
            authorities.add(grantedAuthority);
        }

        return authorities;
    }

    @Override
    public String getPassword() {
        // TODO Auto-generated method stub
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        // TODO Auto-generated method stub
        return user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        // TODO Auto-generated method stub
        // return user.isActive();
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        // TODO Auto-generated method stub
        // return user.isActive();
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        // TODO Auto-generated method stub
        // return user.isActive();
        return true;
    }

    @Override
    public boolean isEnabled() {
        // TODO Auto-generated method stub
        // return user.isActive();
        return true;
    }
    
}
