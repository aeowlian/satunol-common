package com.satunol.common.security.model;

import java.util.LinkedList;
import java.util.List;

import com.satunol.common.security.entities.User.User;
import com.satunol.common.security.entities.User.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Company {

    @Autowired
    private UserRepository userRepository;
    
    public List<User> getSubordinate(String username) {
        List<User> subordinates = new LinkedList<>();
        subordinates.add(userRepository.findById(569).get());
        subordinates.add(userRepository.findById(466).get());
        subordinates.add(userRepository.findById(392).get());
        return subordinates;
    }
}
