package com.satunol.common.security.controller;

import java.io.UnsupportedEncodingException;

import com.satunol.common.security.entities.User.User;
import com.satunol.common.security.entities.User.UserRepository;
import com.satunol.common.security.model.AuthenticationRequest;
import com.satunol.common.security.model.AuthenticationResponse;
import com.satunol.common.security.model.StoredUserDetail;
import com.satunol.common.security.model.TokenObject;
import com.satunol.common.security.repository.JwtRepository;
import com.satunol.common.security.service.JwtService;
import com.satunol.common.security.service.JwtUserDetailsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
public class LoginController {

    @Autowired
    private JwtUserDetailsService myUserDetailService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private JwtRepository jwtRepository;

    @Autowired
    private UserRepository userRepository;

    //TODO: move to config/properties/yml file
    // @Value("http://http://server-10.ddns.net:12220/oauth2/authorization/")
    @Value("http://server-10.ddns.net:12220/oauth2/authorization/")
    private String serverBaseUrl;

    //Code will be appended to the link as get param
    // @Value("http://localhost:4200/#/pages/cold-storage/dashboard")
    @Value("http://localhost:4200/#/onLogin")
    private String frontendRedirectUrl;

    /**
     * Method to get JWT, assuming authorization {@code code} is received
     * @param code
     * @return {@code AuthenticationResponse} containing jwt
     */
    @GetMapping("/authorize")
    public ResponseEntity<?> getJwt(@RequestParam String code) {
        if (code.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        TokenObject jwt = jwtRepository.findByNonce(code).get();
        final AuthenticationResponse authenticationResponse = new AuthenticationResponse(jwt.getJwtString());
        jwtRepository.delete(jwt);
        return ResponseEntity.ok(authenticationResponse);
    }

    /**
     * Method to initiate authentication
     * @param provider
     * @return 302 redirect to {@code provider}'s login screen
     */
    @GetMapping("/authenticate/{provider}")
    public ResponseEntity<?> initiateLogin(@PathVariable String provider) {
        String authUrl = serverBaseUrl + provider;
        // return ResponseEntity.status(HttpStatus.FOUND).header("Location",
        // authUrl).build();
        // return ResponseEntity.status(HttpStatus.OK).header("Location", "http://192.168.4.20:4200").body("{\"url\": \"" + authUrl + "\"}");
        // return ResponseEntity.status(HttpStatus.FOUND).header("Location", "http://192.168.4.20:4200").build();
        return ResponseEntity.status(HttpStatus.FOUND).header("Location", authUrl).build();
    }

    /**
     * Normal login using REST
     * @param authenticationRequest containing username and password
     * @return
     * @throws Exception
     */
    @PostMapping("/token")
    public ResponseEntity<?> login(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {
        // return
        // myUserDetailService.loadUserByUsername(authenticationRequest.getUsername());
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    authenticationRequest.getUsername(), authenticationRequest.getPassword()));
        } catch (Exception e) {
            // TODO: handle exception
            // throw new Exception("Username or password is wrong.");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).header("Content-type", "application/json")
                    .body("{\"message\": \"Username or password is wrong\"}");
        }
        final UserDetails userDetails = myUserDetailService.loadUserByUsername(authenticationRequest.getUsername());
        StoredUserDetail storedUserDetail = (StoredUserDetail) userDetails;
        final String jwt = jwtService.generateToken(userDetails, storedUserDetail.getUser().getEmail());
        final AuthenticationResponse authenticationResponse = new AuthenticationResponse(jwt);
        return ResponseEntity.ok(authenticationResponse);
    }

    /**
     * Successful login will be redirected (as default) to this URI
     * @param principal
     * @return
     * @throws UnsupportedEncodingException
     */
    @GetMapping("/")
    public ResponseEntity<?> def(@AuthenticationPrincipal OidcUser principal) throws UnsupportedEncodingException {
        if (principal == null) {
            return ResponseEntity.notFound().build();
        }
        
        // String nonce = principal.getIdToken().getNonce();

        final UserDetails userDetails = myUserDetailService.loadUserByUsername(principal.getEmail());
        final String jwt = jwtService.generateToken(userDetails, principal.getEmail());

        // TokenObject token = new TokenObject();
        // token.setNonce(nonce);
        // token.setJwtString(jwt);

        // jwtRepository.save(token);

        // return ResponseEntity.ok(new AuthenticationResponse(jwt));

        return ResponseEntity.status(HttpStatus.FOUND)
            .header("Location", frontendRedirectUrl 
                                + "?user=" + userDetails.getUsername()
                                + "&jwt=" + jwt + "&email="
                                + principal.getEmail())
            .build();
    }

    // @GetMapping("/authenticated")
    // public String def1() {
    //     return "<h1>authenticated</h1>";
    // }

    // @GetMapping("/authorized")
    // public String def2() {
    //     return "<h1>authorized</h1>";
    // }

    @GetMapping("/unauthorized")
    public ResponseEntity<?> unauthorized() {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }
}
