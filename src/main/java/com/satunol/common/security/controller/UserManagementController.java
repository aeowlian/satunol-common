package com.satunol.common.security.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.UnaryOperator;

import com.nimbusds.oauth2.sdk.Response;
import com.satunol.common.security.entities.Permission.Permission;
import com.satunol.common.security.entities.Permission.PermissionRepository;
import com.satunol.common.security.entities.User.User;
import com.satunol.common.security.entities.User.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/user")
public class UserManagementController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PermissionRepository permissionRepository;

    @GetMapping("")
    // @PreAuthorize("@authorityEvaluator.hasPermission('read|create|update|delete:user:*')")
    public ResponseEntity<?> getAllUsers() {
        List<User> listOfUsers = userRepository.findAll();
        return ResponseEntity.ok(listOfUsers);
    }

    @GetMapping("/getById/{id}")
    // @PreAuthorize("@authorityEvaluator.hasPermission('read|create|update|delete:user:*')")
    public ResponseEntity<?> getUserById(@PathVariable int id) {
        User user = userRepository.findById(id).get();
        return ResponseEntity.ok(user);
    }

    @GetMapping("/getByEmail/{email}")
    // @PreAuthorize("@authorityEvaluator.hasPermission('read|create|update|delete:user:*')")
    public ResponseEntity<?> getUserByEmail(@PathVariable String email) {
        User user = userRepository.findByEmail(email);
        return ResponseEntity.ok(user);
    }

    @PostMapping("")
    // @PreAuthorize("@authorityEvaluator.hasPermission('create|update:user:*')")
    public ResponseEntity<?> createUser(@RequestBody User user) {
        replacePermission(user, true);
        userRepository.save(user);
        userRepository.flush();
        return ResponseEntity.ok(userRepository.findByEmail(user.getEmail()));
    }

    @PutMapping("")
    // @PreAuthorize("@authorityEvaluator.hasPermission('update:user:*')")
    public ResponseEntity<?> updateUser(@RequestBody User user) {
        replacePermission(user, true);
        userRepository.save(user);
        userRepository.flush();
        return ResponseEntity.ok(user);
    }

    @DeleteMapping("/{id}")
    // @PreAuthorize("@authorityEvaluator.hasPermission('delete:user:*')")
    public ResponseEntity<?> deleteUser(@PathVariable int id) {
        userRepository.deleteById(id);
        userRepository.flush();
        return ResponseEntity.noContent().build();
    }

    // /**
    //  * Assign authority to user by userrole
    //  * accept email and user role from frontend
    //  */
    // @PostMapping("/assign-authority/user-role")
    // @PreAuthorize("@authorityEvaluator.hasPermission('update:user:*')")
    // public ResponseEntity<?> assignAuthorityByUserRole(@RequestBody AddAuthorityRequest request) {
    //     User user = userRepository.findByEmail(request.email);
    //     Optional<JobRole> optionalUserRole = userRoleRepository.findById(request.authorityToBeAdded);
    //     if(!optionalUserRole.isPresent()) {
    //         return ResponseEntity.notFound().build();
    //     }else{
    //         JobRole userRole = optionalUserRole.get();
    //         Authorities currentAuthorities = user.getAuthorities();
    //         Authorities toBeAdded = userRole.getAuthorities();
    //         List<Permission> permissionsToBeAdded = toBeAdded.getPermissions();
    //         Iterator<Permission> i = permissionsToBeAdded.iterator();
    //         while(i.hasNext()) {
    //             Permission p = i.next();
    //             currentAuthorities.addPermission(p);
    //         }
    //         userRepository.save(user);
    //     }
    //     return ResponseEntity.ok(user);
    // }

    // @PostMapping("/assign-authority/permission")
    // @PreAuthorize("@authorityEvaluator.hasPermission('update:user:*')")
    // public ResponseEntity<?> assignAuthority(@RequestBody AddAuthorityRequest request) {
    //     User user = userRepository.findByEmail(request.email);
    //     Optional<Permission> optionalPermission = permissionRepository.findById(request.authorityToBeAdded);
    //     if(!optionalPermission.isPresent()) {
    //         return ResponseEntity.notFound().build();
    //     }else{
    //         Authorities currentAuthorities = user.getAuthorities();
    //         currentAuthorities.addPermission(optionalPermission.get());
    //         userRepository.save(user);
    //     }
    //     return ResponseEntity.ok(user);
    // }

    private void replacePermission(User user, boolean isCreating) {
        ArrayList<Permission> toBeAdded = new ArrayList<>();
        List<Permission> permissionList = user.getAuthorities().getPermissions();
        Iterator<Permission> iterator = permissionList.iterator();
        while(iterator.hasNext()) {
            Permission permission = iterator.next();
            Optional<Permission> permissionString = permissionRepository.findByPermission(permission.getPermission());
            if(!(permissionString.isPresent()) && isCreating) {
                permissionRepository.save(permission);
                permissionRepository.flush();
            }
            iterator.remove();
            if(isCreating) {
                Permission savedPermission = permissionRepository.findByPermission(permission.getPermission()).get();
                toBeAdded.add(savedPermission);
            } else {
                toBeAdded.add(permission);
            }
        }
        for (Permission permission : toBeAdded) {
            permissionList.add(permission);
        }
    }
}

class AddAuthorityRequest {
    final String email;
    final int authorityToBeAdded;

    public AddAuthorityRequest(String email, int authorityToBeAdded) {
        this.email = email;
        this.authorityToBeAdded = authorityToBeAdded;
    }
}

class CreateUserRequest {
    User user;
    int partyId;
}