package com.satunol.common.security.repository;

import java.util.Optional;

import com.satunol.common.security.model.TokenObject;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JwtRepository extends JpaRepository<TokenObject, String>{
    public Optional<TokenObject> findByNonce(String nonce);
}
