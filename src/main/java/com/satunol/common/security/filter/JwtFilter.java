package com.satunol.common.security.filter;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.satunol.common.security.entities.Party.PartyRepository;
import com.satunol.common.security.service.JwtService;
import com.satunol.common.security.service.JwtUserDetailsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import io.jsonwebtoken.Claims;

/**
 * This filter will scan for JWT in {@code Authentication} header.
 * The JWT will be used for authentication and authorization purposes and
 * is required for every transaction; that means the user is not authenticated
 * nor authorized if JWT is not present in the request header.
 */
@Component
public class JwtFilter extends OncePerRequestFilter {

    @Autowired
    private JwtService jwtService;

    @Autowired
    private JwtUserDetailsService userDetailsService;

    @Autowired(required = false)
    private PartyRepository partyRepo;

    @Override
    protected void doFilterInternal(
        HttpServletRequest request,
        HttpServletResponse response,
        FilterChain filterChain
    ) throws ServletException, IOException {
        final String header = request.getHeader("Authorization");
        String token = null;
        String username = null;
        String email = null;

        if(header != null && header.startsWith("Bearer ")) {
            token = header.substring(7);
            username = jwtService.extractUsername(token);
            email = jwtService.extractClaim(token, new Function<Claims,String>(){

                @Override
                public String apply(Claims claims) {
                    String email = claims.get("email", String.class);
                    return email;
                }

            });
        }else{
            // SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            SecurityContextHolder.getContext().setAuthentication(null);
        }

        if(username != null) {
            // UserDetails userDetails = userDetailsService.loadUserByUsername(username);
            boolean isTokenValid = jwtService.validateToken(token, username);

            if(isTokenValid) {
                Collection<? extends GrantedAuthority> authorities = jwtService.extractClaim(token, new Function<Claims,List<GrantedAuthority>>(){

                    @Override
                    public List<GrantedAuthority> apply(Claims claims) {
                        List<GrantedAuthority> permissionList = new LinkedList<GrantedAuthority>();
                        Iterator<Object> iterator = claims.get("permissions", List.class).iterator();
    
                        while(iterator.hasNext()) {
                            LinkedHashMap authorityData = (LinkedHashMap) iterator.next();
                            permissionList.add(new SimpleGrantedAuthority((String)authorityData.get("authority")));
                        }
                        return permissionList;
                    }
                    
                });
                UsernamePasswordAuthenticationToken authToken = 
                    new UsernamePasswordAuthenticationToken(username, partyRepo.findByEmail(email), authorities);
                authToken.setDetails(userDetailsService.loadUserByUsername(email));
                SecurityContextHolder.getContext().setAuthentication(authToken);
            }
        }
        filterChain.doFilter(request, response);
    }

    /**
     * This method is required to persist authentication principal throughout the
     * redirection process to acquire jwt.
     */
    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        if(request.getRequestURI().equals("/authorize") ||
            request.getRequestURI().equals("/") ||
            request.getRequestURI().equals("/favicon.ico") ||
            request.getRequestURI().contains("/login")
        ){
            return true;
        }
        return super.shouldNotFilter(request);
    }
    
}
