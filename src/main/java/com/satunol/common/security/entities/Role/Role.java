package com.satunol.common.security.entities.Role;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.satunol.common.security.entities.Party.Party;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Basic
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
    private Date startDate;

    @Basic
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
    private Date endDate;

    @ManyToOne(targetEntity = Party.class)
    private Party party;


    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Party getParty() {
        return this.party;
    }

    public void setParty(Party party) {
        this.party = party;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}