package com.satunol.common.security.entities.Person;

import javax.persistence.Basic;
import javax.persistence.Entity;

import com.satunol.common.security.entities.Party.Party;

@Entity
public class Person extends Party {
    @Basic
    private Gender gender;

    // @Basic
    // private String fullname;

    public Gender getGender() {
        return gender;
    }

    // public String getFullname() {
    //     return fullname;
    // }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    // public void setFullname(String fullname) {
    //     this.fullname = fullname;
    // }
}
