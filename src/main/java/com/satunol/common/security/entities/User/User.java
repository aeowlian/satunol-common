package com.satunol.common.security.entities.User;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

import com.satunol.common.security.entities.Authorities.Authorities;
import com.satunol.common.security.entities.Role.Role;

@Entity
public class User extends Role {
    // @Id()
    @Column(unique = true)
    @Basic
    private String email;

    @Column(unique = true)
    @Basic
    private String username;

    @Basic
    private String password;

    @OneToOne(targetEntity = Authorities.class, cascade = CascadeType.ALL)
    private Authorities authorities;

    public String getEmail() {
        return this.email;
    }

    public Authorities getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Authorities authorities) {
        this.authorities = authorities;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
