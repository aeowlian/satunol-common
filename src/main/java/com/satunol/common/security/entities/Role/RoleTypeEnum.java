package com.satunol.common.security.entities.Role;

public enum RoleTypeEnum {
    ADMIN,
    SUPERADMIN
}