package com.satunol.common.security.entities.PermissionTemplate;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PermissionTemplateRepository extends JpaRepository<PermissionTemplate, Integer> {
    Optional<PermissionTemplate> findByTemplate(String template);
}
