package com.satunol.common.security.entities.JobRole;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import com.satunol.common.security.entities.PermissionTemplate.PermissionTemplate;
import com.satunol.common.security.entities.PermissionTemplate.PermissionTemplateRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/job-role")
@CrossOrigin(origins = "*")
public class JobRoleController {

    @Autowired
    private PermissionTemplateRepository permissionTemplateRepository;
    
    @Autowired
    private JobRoleRepository jobRoleRepository;
    
    @GetMapping("")
    public ResponseEntity<?> getAll() {
        List<JobRole> jobRoles = jobRoleRepository.findAll();
        return ResponseEntity.ok(jobRoles);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable int id) {
        JobRole jobRole = jobRoleRepository.findById(id).get();
        return ResponseEntity.ok(jobRole);
    }

    @PostMapping("")
    public ResponseEntity<?> create(@RequestBody JobRole jobRole) {
        replacePermission(jobRole, true);
        jobRoleRepository.saveAndFlush(jobRole);
        return ResponseEntity.ok(jobRole);
    }

    @PutMapping("")
    public ResponseEntity<?> update(@RequestBody JobRole jobRole) {
        // replacePermission(jobRole, true);
        jobRoleRepository.save(jobRole);
        jobRoleRepository.flush();
        return ResponseEntity.ok(jobRole);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable int id) {
        jobRoleRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    private void replacePermission(JobRole jobRole, boolean isCreating) {
        ArrayList<PermissionTemplate> toBeAdded = new ArrayList<>();
        List<PermissionTemplate> permissionList = jobRole.getPermissions();
        Iterator<PermissionTemplate> iterator = permissionList.iterator();
        while(iterator.hasNext()) {
            PermissionTemplate permission = iterator.next();
            Optional<PermissionTemplate> permissionString = permissionTemplateRepository.findByTemplate(permission.getTemplate());
            if(!(permissionString.isPresent()) && isCreating) {
                permissionTemplateRepository.save(permission);
                permissionTemplateRepository.flush();
            }
            iterator.remove();
            if(isCreating) {
                PermissionTemplate savedPermission = permissionTemplateRepository.findByTemplate(permission.getTemplate()).get();
                toBeAdded.add(savedPermission);
            } else {
                toBeAdded.add(permission);
            }
        }
        for (PermissionTemplate permission : toBeAdded) {
            permissionList.add(permission);
        }
    }
}
