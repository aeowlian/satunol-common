package com.satunol.common.security.entities.Permission;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/permission")
public class PermissionController {

    @Autowired
    private PermissionRepository permissionRepository;
    
    @GetMapping("")
    public ResponseEntity<?> getAll() {
        List<Permission> permissions = permissionRepository.findAll();
        return ResponseEntity.ok(permissions);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable int id) {
        Permission permission = permissionRepository.findById(id).get();
        return ResponseEntity.ok(permission);
    }

    @PostMapping("")
    public ResponseEntity<?> create(@RequestBody Permission permission) {
        permissionRepository.saveAndFlush(permission);
        return ResponseEntity.ok(permission);
    }

    @PutMapping("")
    public ResponseEntity<?> update(@RequestBody Permission permission) {
        permissionRepository.saveAndFlush(permission);
        return ResponseEntity.ok(permission);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable int id) {
        permissionRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
