package com.satunol.common.security.entities.Authorities;

import java.util.Iterator;
import java.util.List;
import java.util.function.UnaryOperator;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.satunol.common.security.entities.Permission.Permission;

@Entity
public class Authorities {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToMany(cascade = {CascadeType.REFRESH, CascadeType.MERGE, CascadeType.DETACH})
    private List<Permission> permissions;

    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }

    public boolean isAllowed(String permission) {
        for (Permission permissionFromList : permissions) {
            if (hasAuthorityMatchingPermission(permissionFromList.getPermission(), permission.toString())) {
                return true;
            }
        }
        return false;
    }

    private boolean hasAuthorityMatchingPermission(String authorityToMatch, String permissionToMatch) {
        String[] splitPermission = permissionToMatch.split(":");
        String[] splitAuthority = authorityToMatch.split(":");

        int minLength = Integer.min(splitPermission.length, splitAuthority.length);

        // pre-processing

        // label, so it can be referenced by continue statement below
        int i;
        firstLoop: for (i = 0; i < minLength; i++) {
            Wildcard authorityWildcard = containsWildcard(splitAuthority[i]);
            Wildcard permissionWildcard = containsWildcard(splitPermission[i]);

            if (splitAuthority[i].equals(splitPermission[i])) {
                continue;
            } else {
                if (authorityWildcard == Wildcard.STAR || permissionWildcard == Wildcard.STAR) {
                    continue;
                } else if (authorityWildcard == Wildcard.OR || permissionWildcard == Wildcard.OR) {
                    String[] authorityStrings = splitAuthority[i].split("\\|");
                    String[] permissionStrings = splitPermission[i].split("\\|");

                    for (int j = 0; j < authorityStrings.length; j++) {
                        // System.out.println(splitPermission[i].equals(authorityStrings[j]));
                        if (splitPermission[i].contains(authorityStrings[j])) {
                            continue firstLoop;
                        }
                    }

                    for (int j = 0; j < permissionStrings.length; j++) {
                        // System.out.println(splitAuthority[i].equals(permissionStrings[j]));
                        if (splitAuthority[i].contains(permissionStrings[j])) {
                            continue firstLoop;
                        }
                    }
                    return false;
                } else {
                    return false;
                }
            }
        }

        return true;
    }

    private enum Wildcard {
        OR, STAR, NONE
    };

    private Wildcard containsWildcard(String string) {
        if (string.equals("*")) {
            return Wildcard.STAR;
        } else if (string.contains("|")) {
            return Wildcard.OR;
        } else {
            return Wildcard.NONE;
        }
    }

    /**
     * Adding permission with the same object will result in the existing permission to
     * be replaced with the new one. Do save to repository after adding permission.
     * @param permission
     */
    public void addPermission(Permission permission) {
        boolean replacedFlag = false;
        Iterator<Permission> iterator = permissions.iterator();
        String[] splitPermission = permission.getPermission().split(":");
        while(iterator.hasNext()) {
            Permission fromList = iterator.next();
            String[] splitPermissionFromList = fromList.getPermission().split(":");
            if(splitPermission[1].equals(splitPermissionFromList[1])) {
                permissions.replaceAll(new UnaryOperator<Permission>(){

                    @Override
                    public Permission apply(Permission target) {
                        if(target.equals(fromList)) {
                            return permission;
                        }
                        return target;
                    }
                    
                });
                replacedFlag = true;
            }
        }
        if(!replacedFlag) {
            permissions.add(permission);
        }
    }
}
