package com.satunol.common.security.entities.Authorities;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/authorities")
public class AuthoritiesController {
    @Autowired
    private AuthoritiesRepository authoritiesRepository;
    
    @GetMapping("")
    public ResponseEntity<?> getAll() {
        List<Authorities> authorities = authoritiesRepository.findAll();
        return ResponseEntity.ok(authorities);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable int id) {
        Authorities authorities = authoritiesRepository.findById(id).get();
        return ResponseEntity.ok(authorities);
    }

    @PostMapping("")
    public ResponseEntity<?> create(@RequestBody Authorities authorities) {
        authoritiesRepository.saveAndFlush(authorities);
        return ResponseEntity.ok(authorities);
    }

    @PutMapping("")
    public ResponseEntity<?> update(@RequestBody Authorities authorities) {
        authoritiesRepository.saveAndFlush(authorities);
        return ResponseEntity.ok(authorities);
    }

    @DeleteMapping("")
    public ResponseEntity<?> delete(@RequestBody Authorities authorities) {
        authoritiesRepository.delete(authorities);
        return ResponseEntity.ok(authorities);
    }
}
