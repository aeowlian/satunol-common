package com.satunol.common.security.entities.PermissionTemplate;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/permission-template")
public class PermissionTemplateController {

    @Autowired
    private PermissionTemplateRepository permissionTemplateRepository;
    
    @GetMapping("")
    public ResponseEntity<?> getAll() {
        List<PermissionTemplate> permissionTemplates = permissionTemplateRepository.findAll();
        return ResponseEntity.ok(permissionTemplates);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable int id) {
        PermissionTemplate permissionTemplate = permissionTemplateRepository.findById(id).get();
        return ResponseEntity.ok(permissionTemplate);
    }

    @PostMapping("")
    public ResponseEntity<?> create(@RequestBody PermissionTemplate permissionTemplate) {
        permissionTemplateRepository.saveAndFlush(permissionTemplate);
        return ResponseEntity.ok(permissionTemplate);
    }

    @PutMapping("")
    public ResponseEntity<?> update(@RequestBody PermissionTemplate permissionTemplate) {
        permissionTemplateRepository.saveAndFlush(permissionTemplate);
        return ResponseEntity.ok(permissionTemplate);
    }

    @DeleteMapping("")
    public ResponseEntity<?> delete(@RequestBody PermissionTemplate permissionTemplate) {
        permissionTemplateRepository.delete(permissionTemplate);
        return ResponseEntity.ok(permissionTemplate);
    }
}
