package com.satunol.common.security.entities.Permission;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * AuthorityRepository
 */
public interface PermissionRepository extends JpaRepository<Permission, Integer> {

    Optional<Permission> findByPermission(String permission);
}