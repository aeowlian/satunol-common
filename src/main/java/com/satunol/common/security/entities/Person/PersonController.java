package com.satunol.common.security.entities.Person;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/person")
@CrossOrigin(origins = "*")
public class PersonController {
    
    @Autowired
    private PersonRepository personRepository;

    @GetMapping("")
    public ResponseEntity<?> getAll() {
        List<Person> listOfPersons = personRepository.findAll();
        return ResponseEntity.ok(listOfPersons);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getPerson(@PathVariable Integer id) {
        Person getPerson = personRepository.findById(id).get();
        return ResponseEntity.ok(getPerson);
    }

    @PostMapping("")
    public ResponseEntity<?> createPerson(@RequestBody Person person) {
        personRepository.save(person);
        personRepository.flush();
        return ResponseEntity.ok(person);
    }

    @PutMapping("")
    public ResponseEntity<?> updatePerson(@RequestBody Person person) {
        personRepository.save(person);
        personRepository.flush();
        return ResponseEntity.ok(person);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletePerson(@PathVariable int id) {
        personRepository.deleteById(id);
        personRepository.flush();
        return ResponseEntity.ok().build();
    }
}
