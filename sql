-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 26, 2020 at 04:17 PM
-- Server version: 5.7.32-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `security`
--

--
-- Truncate table before insert `authorities`
--
INSERT INTO `permission` (`id`, `permission`) VALUES
(1, 'read:performance:*'),
(2, 'create|update|delete:location:*'),
(3, '*:*:*');
--
-- Dumping data for table `authorities`
--

INSERT INTO `authorities` (`id`) VALUES
(1);

--
-- Truncate table before insert `authorities_permission_list`
--

--
-- Dumping data for table `authorities_permission_list`
--

INSERT INTO `authorities_permission_list` (`authorities_id`, `permission_list_id`) VALUES
(1, 3);

--
-- Truncate table before insert `hibernate_sequence`
--

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(4);

--
-- Truncate table before insert `inventory`
--

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`id`, `height`, `length`, `types`, `weight`, `width`) VALUES
(1, 103, 103, 'FISH\nMEAT\nVEGETABLES\nFRUITS', 103, 103);

--
-- Truncate table before insert `location`
--

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `address`, `latitude`, `longitude`, `name`) VALUES
(1, 'test address', '42.32', '76.41', 'test location');

--
-- Truncate table before insert `managed_object`
--

--
-- Dumping data for table `managed_object`
--

INSERT INTO `managed_object` (`id`, `brand`, `creation_date`, `name`, `native_id`, `operation_date`, `product_number`, `serial_number`, `stop_date`, `type`, `location_id`) VALUES
(1, 'Samsung', '2020-11-09 00:00:00', 'AC Lt. 1', 'AC0001', '2020-11-09 00:00:00', 'AC0001', 'AC0001', '2022-03-31 00:00:00', 'Air Conditioner', 1),
(2, 'Samsung', '2020-11-09 00:00:00', 'AC Lt. 2', 'AC001', '2020-11-09 00:00:00', 'AC001', 'AC001', '2022-05-19 00:00:00', 'Air Conditioner', 1),
(3, 'Samsung', '2020-11-09 00:00:00', 'Air Purifier Lt. 2', 'AP001', '2020-11-09 00:00:00', 'AP001', 'AP001', '2022-05-19 00:00:00', 'Air Purifier', 1);

--
-- Truncate table before insert `party`
--

--
-- Truncate table before insert `perf_measurement`
--

--
-- Dumping data for table `perf_measurement`
--
INSERT INTO `perf_object` (`id`, `name`, `type`, `unit`) VALUES
(1, 'Readings 1', 'Type1', 'Celcius'),
(2, 'Readings 2', 'Type2', 'Celcius');
INSERT INTO `perf_measurement` (`id`, `perf_measurement_time`, `perf_measurement_value`, `details_id`, `managed_object_id`) VALUES
(1, '2020-11-10 00:00:00', '18', 1, 1),
(2, '2020-11-10 00:00:00', '18', 1, 2),
(3, '2020-11-10 00:00:00', '18', 1, 3),
(4, '2020-11-10 00:00:00', '18', 2, 1),
(5, '2020-11-10 00:00:00', '18', 2, 2),
(6, '2020-11-10 00:00:00', '18', 2, 3);

--
-- Truncate table before insert `perf_object`
--

--
-- Dumping data for table `perf_object`
--



--
-- Truncate table before insert `permission`
--

--
-- Dumping data for table `permission`
--



--
-- Truncate table before insert `role`
--

--
-- Truncate table before insert `token`
--

--
-- Truncate table before insert `user`
--

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`email`, `password`, `username`, `authorities_id`) VALUES
('yehezkieljason@gmail.com', 'qwerasdf', 'jasonyehezkiel_', 1);

--
-- Truncate table before insert `user_role`
--












============================================================================================
-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 04, 2020 at 10:24 AM
-- Server version: 5.7.32-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `security`
--

--
-- Dumping data for table `authorities`
--

INSERT INTO `permission` (`id`, `permission`) VALUES
(3, '*:*:*'),
(35, 'create:alarm:JawaBarat'),
(2999, 'create|update|delete:location:*'),
(1999, 'read:performance:*');

INSERT INTO `authorities` (`id`) VALUES
(1),
(37),
(39);

--
-- Dumping data for table `authorities_permissions`
--

INSERT INTO `authorities_permissions` (`authorities_id`, `permissions_id`) VALUES
(1, 3),
(37, 3),
(37, 35),
(39, 3),
(39, 35);

--
-- Dumping data for table `authorities_permission_list`
--


--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(43),
(43);

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`id`, `height`, `length`, `types`, `weight`, `width`) VALUES
(1, 103, 103, 'FISH\nMEAT\nVEGETABLES\nFRUITS', 103, 103);

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `address`, `latitude`, `longitude`, `name`) VALUES
(1, 'test address', '42.32', '76.41', 'test location');

--
-- Dumping data for table `managed_object`
--

INSERT INTO `managed_object` (`id`, `brand`, `creation_date`, `name`, `native_id`, `operation_date`, `product_number`, `serial_number`, `stop_date`, `type`, `location_id`) VALUES
(1, 'Samsung', '2020-11-09 00:00:00', 'AC Lt. 1', 'AC0001', '2020-11-09 00:00:00', 'AC0001', 'AC0001', '2022-03-31 00:00:00', 'Air Conditioner', 1),
(2, 'Samsung', '2020-11-09 00:00:00', 'AC Lt. 2', 'AC001', '2020-11-09 00:00:00', 'AC001', 'AC001', '2022-05-19 00:00:00', 'Air Conditioner', 1),
(3, 'Samsung', '2020-11-09 00:00:00', 'Air Purifier Lt. 2', 'AP001', '2020-11-09 00:00:00', 'AP001', 'AP001', '2022-05-19 00:00:00', 'Air Purifier', 1);

--
-- Dumping data for table `party`
--

INSERT INTO `party` (`id`, `address`, `email`, `phone`, `birthdate`, `fullname`) VALUES
(1, 'some address', 'yehezkieljason@gmail.com', '081108110811', NULL, NULL),
(2, 'some address', 'jasonyehezkiel@yahoo.com', '08108110811', '2020-12-01 00:00:00', 'jyjyjy');

--
-- Dumping data for table `perf_measurement`
--
INSERT INTO `perf_object` (`id`, `name`, `type`, `unit`) VALUES
(1, 'Readings 1', 'Type1', 'Celcius'),
(2, 'Readings 2', 'Type2', 'Celcius');


INSERT INTO `perf_measurement` (`id`, `perf_measurement_time`, `perf_measurement_value`, `details_id`, `managed_object_id`) VALUES
(1, '2020-11-10 00:00:00', '18', 1, 1),
(2, '2020-11-10 00:00:00', '18', 1, 2),
(3, '2020-11-10 00:00:00', '18', 1, 3),
(4, '2020-11-10 00:00:00', '18', 2, 1),
(5, '2020-11-10 00:00:00', '18', 2, 2),
(6, '2020-11-10 00:00:00', '18', 2, 3);

--
-- Dumping data for table `perf_object`
--


--
-- Dumping data for table `permission`
--


--
-- Dumping data for table `permission_template`
--

INSERT INTO `permission_template` (`id`, `template`) VALUES
(1, 'create|update|delete:location:*'),
(2, 'read:performance:<location>');

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`gender`, `id`) VALUES
(1, 1),
(1, 2);

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `end_date`, `start_date`, `party_id`) VALUES
(1, '2021-08-19 00:00:00', '2020-12-01 00:00:00', 1),
(36, '2021-08-19 00:00:00', '2020-12-01 00:00:00', 2),
(38, '2021-08-19 00:00:00', '2020-12-01 00:00:00', 2);

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`email`, `password`, `username`, `id`, `authorities_id`) VALUES
('yehezkieljason@gmail.com', 'qweradsf', 'aeowlian', 1, 1),
('jasonyehezkiel@yahoo.com', 'qweradsf', 'jy006', 38, 39);


INSERT INTO `job_role` (`id`, `name`) VALUES
(7, 'ADMIN'),
(10, 'ADMIN_OTHER');

--
-- Dumping data for table `job_role_permissions`
--
INSERT INTO `permission_template` (`id`, `template`) VALUES
(2, 'read:performance'),
(9, 'update:user');

INSERT INTO `job_role_permissions` (`job_role_id`, `permissions_id`) VALUES
(10, 2),
(10, 9);
